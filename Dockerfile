ARG DEBIAN_VERSION=buster

FROM debian:$DEBIAN_VERSION

ARG DOCKER_VERSION
ARG DOCKER_COMPOSE_VERSION
ARG NODE_VERSION
ARG DOCKER_COMPOSE_MERGE_VERSION
ARG STACKD_VERSION

ENV DOCKER_VERSION=${DOCKER_VERSION:-19.03.2}
ENV DOCKER_COMPOSE_VERSION=${DOCKER_COMPOSE_VERSION:-1.24.1}
ENV NODE_VERSION=${NODE_VERSION:-12.11.0}
ENV DOCKER_COMPOSE_MERGE_VERSION=${DOCKER_COMPOSE_MERGE_VERSION:-latest}
ENV STACKD_VERSION=${STACKD_VERSION:-latest}

RUN apt-get update && apt-get install -y \
 netcat \
 httpie \
 jq \
 bash \
 curl \
 python3 \
 python3-distutils \
 && rm -rf /var/lib/apt/lists/*

# install pip
RUN cd /tmp &&\
  curl -L https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py &&\
  python3 /tmp/get-pip.py &&\
  rm /tmp/get-pip.py

# install docker
RUN curl -L https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz -o /tmp/docker.tgz \
  && cd /tmp && tar xzvf /tmp/docker.tgz \
  && cp docker/* /usr/bin/ \
  && rm -rf docker && rm -f docker.tgz

# install docker-compose
RUN curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose

# install nodejs
ENV NODE_DISTRO=linux-x64
RUN curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-${NODE_DISTRO}.tar.xz -o /tmp/node.tar.xz \
  && mkdir -p /usr/local/lib/nodejs \
  && tar -xJvf /tmp/node.tar.xz -C /usr/local/lib/nodejs \
  && ln -s /usr/local/lib/nodejs/node-v$NODE_VERSION-$NODE_DISTRO/bin/node /usr/bin/node \
  && ln -s /usr/local/lib/nodejs/node-v$NODE_VERSION-$NODE_DISTRO/bin/npm /usr/bin/npm \
  && ln -s /usr/local/lib/nodejs/node-v$NODE_VERSION-$NODE_DISTRO/bin/npx /usr/bin/npx

# docker-compose-merge
RUN npm i -g docker-compose-merge@$DOCKER_COMPOSE_MERGE_VERSION \
  && ln -s /usr/local/lib/nodejs/node-v$NODE_VERSION-$NODE_DISTRO/bin/docker-compose-merge /usr/bin/docker-compose-merge

# install stackd
ENV PATH=$PATH:/root/.local/bin
RUN [ "$STACKD_VERSION" = "latest" ] && pip install --user stackd \
  || pip install --user stackd==$STACKD_VERSION

# pwd
RUN mkdir -p /var/run/stackd-pwd
WORKDIR /var/run/stackd-pwd

COPY docker-container/bin/ /usr/local/bin/

ENTRYPOINT ["entrypoint.sh"]
CMD []